# 雪花算法生成id

#### 介绍
id生成的六种方法：雪花算法

#### 软件架构
软件架构说明
https://blog.csdn.net/u010398771/article/details/79765836
java常用两种：
1.uuid/guid
2.snowflake雪花算法

常见六种方式生成id：
方法一: 用数据库的 auto_increment 来生成
方法二：单点批量ID生成服务
方法三：uuid / guid
    不管是通过数据库，还是通过服务来生成ID，业务方Application都需要进行一次远程调用，比较耗时。uuid是一种常见的本地生成ID的方法。
    UUID uuid = UUID.randomUUID();
方法四：取当前毫秒数
方法五：使用 Redis 来生成 id
方法六：Twitter 开源的 Snowflake 算法

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
